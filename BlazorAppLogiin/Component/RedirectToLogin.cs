﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorAppLogiin.Component
{
    public class RedirectToLogin : ComponentBase
    {
        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        private readonly string LOGIN_URL = "/Identity/Account/Login";

        protected override void OnInitialized()
        {
            var returnUrl = NavigationManager.ToBaseRelativePath(NavigationManager.Uri);
            if (string.IsNullOrEmpty(returnUrl))
            {
                NavigationManager.NavigateTo(this.LOGIN_URL, true);
                return;
            }
            returnUrl = string.Concat("~/",returnUrl);
            NavigationManager.NavigateTo($"{this.LOGIN_URL}?returnUrl={returnUrl}", forceLoad: true);
        }
    }
}
