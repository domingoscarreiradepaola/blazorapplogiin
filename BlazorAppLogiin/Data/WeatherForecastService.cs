using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace BlazorAppLogiin.Data
{
    public class WeatherForecastService
    {
        private readonly int TOTAL_RECORDS = 10;
        private readonly int START_ID = 1;
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public static IEnumerable<WeatherForecast> Forecasts;

 
        public Task initializeForecasts()
        {
            Forecasts = Enumerable.Range(this.START_ID, this.TOTAL_RECORDS).Select(index => new WeatherForecast
            {
                Id = index,
                Date = DateTime.Now.AddDays(index),
                TemperatureC = (new Random()).Next(-20, 55),
                Summary = Summaries[(new Random()).Next(Summaries.Length)]
            });
            return Task.FromResult(true);
        }
        public Task<IEnumerable<WeatherForecast>> GetForecastAsync(CancellationToken ct = default)
        {
            return Task.FromResult(Forecasts);
        }

        public Task Insert(IDictionary<string, object> values)
        {
            WeatherForecast dataItem = new WeatherForecast();
            dataItem.Id = Forecasts.Max(i => i.Id) + 1;
            this.setProprietyValues(dataItem, values);
            Forecasts = Forecasts.Append(dataItem);
            return Task.CompletedTask;
        }

        public Task Update(WeatherForecast dataItem, IDictionary<string, object> newValue)
        {
            this.setProprietyValues(dataItem, newValue);
            Forecasts = Forecasts.Select((item) => item.Id == dataItem.Id ? dataItem : item);
            return Task.CompletedTask;
        }

        public Task Remove(WeatherForecast dataItem)
        {
            Forecasts = Forecasts.Where(item => item.Id != dataItem.Id);
            return Task.CompletedTask;
        }

        private void setProprietyValues(WeatherForecast dataItem, IDictionary<string, object>  values)
        {
            foreach (var field in values.Keys)
            {
                PropertyInfo proprieties = dataItem.GetType().GetProperty(field);
                switch (Type.GetTypeCode(proprieties.PropertyType))
                {
                    case TypeCode.Int32:
                        proprieties.SetValue(dataItem, Convert.ToInt32(values[field]));
                        break;
                    case TypeCode.String:
                        proprieties.SetValue(dataItem, values[field]);
                        break;
                    case TypeCode.DateTime:
                        proprieties.SetValue(dataItem, Convert.ToDateTime(values[field]));
                        break;
                }
            }
        }

    }
}
